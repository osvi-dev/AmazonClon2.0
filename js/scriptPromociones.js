

function scrollCarouselhistbusq(amount) {
    event.preventDefault(); // Prevenir el comportamiento predeterminado del botón
    const carousel = document.querySelector('.carousel-histbusq');
    carousel.scrollBy({
      left: amount,
      behavior: 'smooth'
    });
  }
  
