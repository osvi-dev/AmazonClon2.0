// Datos de ejemplo para los productos
const products = [
  {
    name: "Xiaomi Smartphone Poco M5",
    image: "../imgs/poco2.jpg",
    price: "$2,727",
    oldPrice: "$2,999",
    ratings: 651,
    shippingDate: "18 de feb"
  },
  {
    name: "motorola Moto E22",
    image: "../imgs/moto.jpg",
    price: "$1,493",
    oldPrice: "$3,999",
    ratings: 83,
    shippingDate: "17 de feb"
  },
  {
    name: "Xiaomi Celular Poco M5 Black",
    image: "../imgs/poco.jpg",
    price: "$2,088",
    oldPrice: "",
    ratings: 2549,
    shippingDate: "18 de feb"
  }
];

// Función para cargar productos
function loadProducts() {
  const container = document.getElementById('productContainer');
  products.forEach(product => {
    container.innerHTML += `
      <div class="product">
        <img src="${product.image}" alt="${product.name}">
        <h3>${product.name}</h3>
        <p class="price">${product.price}</p>
        <p class="old-price">${product.oldPrice}</p>
        <p>${product.ratings} valoraciones</p>
        <p>Envío GRATIS el ${product.shippingDate}</p>
        <div>
          <input type="hidden" id="quantity-${product.name}" value="1" min="1">
          <button onclick="addToCart('${product.name}', '${product.image}', '${product.ratings}', '${product.price}', '${product.shippingDate}')">Agregar al carrito</button>
        </div>
      </div>
    `;
  });
}

// Función para agregar al carrito
function addToCart(name, image, ratings, price, shippingDate) {
  const quantityInput = document.getElementById(`quantity-${name}`);
  const quantity = parseInt(quantityInput.value);

  let cart = JSON.parse(localStorage.getItem('cart')) || [];
  const existingItemIndex = cart.findIndex(item => item.name === name);

  if (existingItemIndex !== -1) {
    cart[existingItemIndex].quantity += quantity;
  } else {
    cart.push({ name, image, ratings, price, shippingDate, quantity });
  }

  localStorage.setItem('cart', JSON.stringify(cart));
}

// Cargar productos al iniciar
window.onload = loadProducts;
