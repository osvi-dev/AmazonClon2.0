// Datos de ejemplo para los productos
const products = [
    {
      name: "Asus ROG Zephyrus Duo 16 (2022) Laptop para Juegos, 16 Pulgadas 165Hz IPS Tipo WUXGA 16:10, NVIDIA GeForce...",
      image: "../imgs/zduo.jpg",
      price: "$47,100.00",
      ratings: 119,
      shippingDate: "Entrega GRATIS entre el 29 de feb - 4 de mar"
    },
    {
      name: "ASUS ROG Zephyrus G16 - Portátil para juegos, WUXGA 16 pulgadas 165Hz, Intel 10-Core i7...",
      image: "../imgs/z16.jpg",
      price: "$32,682.79",
      ratings: 7,
      shippingDate: "Entrega por $274.01 el mié, 21 de feb"
    },
    {
      name: "ASUS 2022 ROG Zephyrus 16 pulgadas FHD 165Hz Laptop Intel Core i7-12700H, NVIDIA GeForce...",
      image: "../imgs/z162.jpg",
      price: "$28,140.51",
      ratings: 3,
      shippingDate: "Entrega por $261.77 el lun, 19 de feb"
    }
  ];
  
  // Función para cargar productos
  function loadProducts() {
    const container = document.getElementById('productContainer');
    products.forEach(product => {
      container.innerHTML += `
        <div class="product">
          <img src="${product.image}" alt="${product.name}">
          <div class="product-info">
            <h3>${product.name}</h3>
            <p class="price">${product.price}</p>
            <div class="ratings">${product.ratings} valoraciones</div>
            <p class="shippingDate">${product.shippingDate}</p>
            <div>
              <input type="hidden" id="quantity-${product.name}" value="1" min="1">
              <button onclick="addToCart('${product.name}', '${product.image}', '${product.ratings}', '${product.price}', '${product.shippingDate}')">Agregar al carrito</button>
            </div>
          </div>
        </div>
      `;
    });
  }

  // Función para agregar al carrito
  function addToCart(name, image, ratings, price, shippingDate) {
  const quantityInput = document.getElementById(`quantity-${name}`);
  const quantity = parseInt(quantityInput.value);

  let cart = JSON.parse(localStorage.getItem('cart')) || [];
  const existingItemIndex = cart.findIndex(item => item.name === name);

  if (existingItemIndex !== -1) {
    cart[existingItemIndex].quantity += quantity;
  } else {
    cart.push({ name, image, ratings, price, shippingDate, quantity });
  }

  localStorage.setItem('cart', JSON.stringify(cart));
}
  
  // Cargar productos al iniciar
  window.onload = loadProducts;
  