// Datos de ejemplo para los productos
const products = [
    {
      name: "La Biblioteca De La Medianoche",
      image: "../imgs/l1.jpg",
      autor: "de Matt Haig",
      price: "$298",
      ratings: 651,
      shippingDate: "18 de feb"
    },
    {
      name: "Antes de que se enfríe el café",
      image: "../imgs/l2.jpg",
      autor: "de Toshikazu Kawaguchi",
      price: "$249",
      ratings: 83,
      shippingDate: "17 de feb"
    },
    {
      name: "Hábitos Atómicos",
      image: "../imgs/l3.jpg",
      autor: "de James Clear",
      price: "$369",
      ratings: 2549,
      shippingDate: "18 de feb"
    }
  ];
  
// Función para cargar productos
function loadProducts() {
  const container = document.getElementById('productContainer');
  products.forEach(product => {
    container.innerHTML += `
      <div class="product">
        <img src="${product.image}" alt="${product.name}">
        <h3>${product.name}</h3>
        <h5>${product.autor}</h5>
        <p class="price">${product.price}</p>
        <p>${product.ratings} valoraciones</p>
        <p>Envío GRATIS el ${product.shippingDate}</p>
        <div>
          <input type="hidden" id="quantity-${product.name}" value="1" min="1">
          <button onclick="addToCart('${product.name}', '${product.image}', '${product.ratings}', '${product.price}', '${product.shippingDate}')">Agregar al carrito</button>
        </div>
      </div>
    `;
  });
}

// Función para agregar al carrito
function addToCart(name, image, ratings, price, shippingDate) {
  const quantityInput = document.getElementById(`quantity-${name}`);
  const quantity = parseInt(quantityInput.value);

  let cart = JSON.parse(localStorage.getItem('cart')) || [];
  const existingItemIndex = cart.findIndex(item => item.name === name);

  if (existingItemIndex !== -1) {
    cart[existingItemIndex].quantity += quantity;
  } else {
    cart.push({ name, image, ratings, price, shippingDate, quantity });
  }

  localStorage.setItem('cart', JSON.stringify(cart));
}

window.onload = loadProducts;

  